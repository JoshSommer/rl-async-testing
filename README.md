Asynchronous test tools for rxjs and Angular

Requires @angular/core since the tooling relies on zones to schedule asynchronous actions

## Installation
Install rl-async-testing from npm:
```
npm install rl-async-testing --save
```
Configure with Angular-CLI and SystemJs:
```
/** Map relative paths to URLs. */
const map: any = {
    'rl-async-testing': 'vendor/rl-async-testing'
};

/** User packages configuration. */
const packages: any = {
    'rl-async-testing': {
        main: 'index.js'
    }
};
```
Modify angular-cli-build.js by adding this line to vendorNpmFiles:
```
'rl-async-testing/**/*.+(js|js.map)'
```

## Usage
The rlFakeAsync zone can be used in place of the angular fakeAsync zone. The mock tools can be used in conjunction or independently of this, but if combined, the zone will track requests against the mocks and fail the test if there are any requests pending.
```
import { rlFakeAsync, mock, IMockedPromise, IMockedRequest } from 'rl-async-testing';

interface IMockDataService {
    getWithPromise: IMockedPromise<number>;
    getWithObservable: IMockedRequest<number>;
}

// mock definition
let dataService: IMockDataService = {
    getWithPromise: mock.promise(3),
    getWithObservable: mock.request(3),
};
// or
let dataService: IMockDataService = {
    getWithPromise: mock.promise(x => x.value),
    getWithObservable: mock.request(x => x.value),
};


it('should schedule mock async requests synchronously', rlFakeAsync(() => {
    dataService.getWithPromise();
    dataService.getWithObservable();
    
    // IMockedPromise and IMockedRequest extend Sinon.SinonSpy
    sinon.assert.calledOnce(getWithPromise);
    sinon.assert.calledOnce(getWithObservable);
    
    // assert data not loaded
    
    dataService.getWithPromise.flush();
    dataService.getWithObservable.flush();
    
    // assert data loaded
}));
```
### rxjs scheduling
The rlFakeAsync zone hooks into the rxjs async scheduler to schedule observables based on 'fake' time. This requires using the rlTick wrapper, which calls into angular's tick, but also tracks time for the rxjs scheduler.
```
import { rlFakeAsync, rlTick, flushMicrotasks } from 'rl-async-testing';

it('should schedule rxjs observables synchronously', rlFakeAsync(() => {
    Observable.of(3).delay(1000);
    
    // assert data not loaded
    
    rlTick(1000);
    flushMicrotasks();
    
    // assert data loaded
}));
```

## Tests
This project uses systemjs and karma to run unit tests.
```
npm install
npm run build
npm test
```
Alternately:
```
npm install
npm run build-test
```
If you encounter an issue and want to debug it:
```
npm run test.debug
```
or
```
npm run build-test.watch
```
(Runs tsc in watch mode and concurrently runs the test debugger)